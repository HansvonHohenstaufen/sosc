/* See LICENSE file for copyright and license details. */

#include <ncurses.h>

#include "drw.h"

#include "config.h"

/* Variables */
static int databefore[N];

static int window_col;
static int window_lin;

static WINDOW *wmain, *wmeas, *wpara;

/* Functions implementations */
void
drw_data(int *data)
{
	int i;

	/* Print data */
	for (i=0; i<window_col; i++)
	{
		mvwaddch(wmain, databefore[i], i+1, ' ');
		mvwaddch(wmain, data[i], i+1, '*');
		databefore[i] = data[i];
	}


	wrefresh(wmain);
}

void
drw_window()
{
	/* Windows */
	wmain = newwin(LINE-(window_parameters/2)-2, COL-window_measures+1,
			0, 0);
	wpara = newwin(window_parameters+1, COL-window_measures+1,
			LINE-window_parameters, 0);
	wmeas = newwin(LINE+1, window_measures, 0, COL-window_measures+1);

	box(wmain, 0, 0);
	box(wpara, 0, 0);
	box(wmeas, 0, 0);

	/* Parameters */
	mvwprintw(wpara, 1, 1, "Time:");
	mvwprintw(wpara, 1, 21, "Voltage:");
	mvwprintw(wpara, 1, 41, "Frequency:");

	/* Measures */
	mvwprintw(wmeas, 1, 2, "Vp:");
	mvwprintw(wmeas, 4, 2, "Vpp:");
	mvwprintw(wmeas, 7, 2, "Vm:");
	mvwprintw(wmeas, 10, 2, "T:");
	mvwprintw(wmeas, 13, 2, "f:");

	mvwprintw(wmeas, 19, 2, "FFT");
	mvwprintw(wmeas, 20, 2, "===");
	mvwprintw(wmeas, 21, 2, "v1:");
	mvwprintw(wmeas, 24, 2, "v2:");
	mvwprintw(wmeas, 27, 2, "v3:");

	/* Print screen */
	wrefresh(wmain);
	wrefresh(wmeas);
	wrefresh(wpara);
}

void
drw_getwindowsize(int *col, int *lin)
{
	*col = window_col;
	*lin = window_lin;
}

void
drw_measure(float vp, float vpp, float vm,
		float f, char *t,
		float v1, float v2, float v3,
		float f2, float f3)
{
	char clean[11];
	char cvp[10], cvpp[10], cvm[10], cf[10],
		cv1[10], cv2[10], cv3[10], cf2[10], cf3[10];

	/* Clean */
	sprintf(clean, "          ");
	mvwaddstr(wmeas, 2, 7, clean);
	mvwaddstr(wmeas, 5, 7, clean);
	mvwaddstr(wmeas, 8, 7, clean);

	mvwaddstr(wmeas, 11, 7, clean);
	mvwaddstr(wmeas, 14, 7, clean);

	mvwaddstr(wmeas, 22, 7, clean);
	mvwaddstr(wmeas, 23, 7, clean);
	mvwaddstr(wmeas, 25, 7, clean);
	mvwaddstr(wmeas, 26, 7, clean);
	mvwaddstr(wmeas, 28, 7, clean);
	mvwaddstr(wmeas, 29, 7, clean);

	/* Print */
	sprintf(cvp,  "%5.3f V", vp);
	sprintf(cvpp, "%5.3f V", vpp);
	sprintf(cvm,  "%5.3f V", vm);
	sprintf(cf,   "%5.1f Hz", f);
	sprintf(cv1,  "%5.3f V", v1);
	sprintf(cv2,  "%5.3f V", v2);
	sprintf(cv3,  "%5.3f V", v3);
	sprintf(cf2,  "%5.1f Hz", f2);
	sprintf(cf3,  "%5.1f Hz", f3);

	cvp[9] = cvpp[9] = cvm[9] = cf[9] = '\0';
	cv1[9] = cv2[9] = cv3[9] = cf2[9] = cf3[9] = '\0';

	mvwaddstr(wmeas, 2, 7, cvp);
	mvwaddstr(wmeas, 5, 7, cvpp);
	mvwaddstr(wmeas, 8, 7, cvm);

	mvwaddstr(wmeas, 14, 7, cf);
	mvwaddstr(wmeas, 11, 7, t);

	mvwaddstr(wmeas, 23, 7, cv1);
	mvwaddstr(wmeas, 26, 7, cv2);
	mvwaddstr(wmeas, 29, 7, cv3);

	mvwaddstr(wmeas, 22, 7, cf);
	mvwaddstr(wmeas, 25, 7, cf2);
	mvwaddstr(wmeas, 28, 7, cf3);

	wrefresh(wmeas);
	refresh();
}

void
drw_parameters(char *time, char *volt, char *f)
{
	mvwaddstr(wpara, 2, 7, time);
	mvwaddstr(wpara, 2, 30, volt);
	mvwaddstr(wpara, 2, 50, f);

	wrefresh(wpara);
}

void
drw_end()
{
	endwin();
}

int
drw_start()
{
	int i;


	/* Init and configuration */
	initscr();		/* Start curses mode       */
	raw();			/* Line buffering disabled */
	noecho();		/* Dont echo               */
	//nodelay(stdscr, 1);
	timeout(100);
	curs_set(0);		/* Hide cursor             */

	/* Check window size */
	if (LINE<(window_parameters+30))
		return 1;
	if (COL<(window_measures+50))
		return 2;

	/* Signal window */
	window_lin = LINE-window_parameters-2;
	window_col = COL-window_measures-1;

	for (i=0; i<N; i++)
		databefore[i] = window_lin;

	refresh();
	drw_window();

	return 0;
}
