/* See LICENSE file for copyright and license details. */

/* Functions declarations */
void serial_clean(void);
void serial_closeport(void);
int serial_openport(const char *Serial_P, const unsigned int Bauds);
int serial_readchar(char *Buffer, unsigned int time_ms);
int serial_writechar(const char *Buffer, const unsigned int NbBytes);
