/* See LICENSE file for copyright and license details. */

#include <complex.h>

#define PI 3.14159265358979323846

/* Functions declarations */
void fft_calculate(float *x, float *y);
