/* See LICENSE file for copyright and license details. */

/* Window size */
static const int window_parameters	= 3;
static const int window_measures	= 20;

/* Voltage max from ADC */
static const float default_voltage	= 3.3;

/* Size from buffer data */
#define N	2048
