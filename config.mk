# sosc version
VERSION = 0.2

# Paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

# Includes and libs
LIBS = -lncurses -ltinfo -lpthread

# Flags
CPPFLAGS = -D_POSIX_C_SOURCE=200809L -DVERSION=\"${VERSION}\"
CFLAGS   = -std=c99 -Os -Wall ${CPPFLAGS}
LDFLAGS  = ${LIBS}

# Compiler
CC = cc
