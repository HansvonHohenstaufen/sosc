/* See LICENSE file for copyright and license details. */

#include <complex.h>
#include <stdio.h>

#include "fft.h"

#include "config.h"

/* Globals */

/* Functions implementations */
void
fft_calculate(float *x, float *y)
{
	int k, i;

	float complex g[N/2];
	float complex h[N/2];

	float complex feh;

	for (k=0; k<(N/2); k++)
	{
		g[k] = 0+0*I;
		h[k] = 0+0*I;
	}

	for (i=1; i<N; i=i+2 )
		for (k=0; k<(N/2); k++)
		{
			feh = 0 + (-4*k*PI*((i-1)/2)/N)*I;
			feh = cexp(feh);
			g[k] += x[i-1]*feh;
			h[k] += x[i]*feh;
		}

	y[0] = cabs(g[0] + (h[0]))/(N);
	for (i=1; i<(N/2); i++)
	{
		feh = 0 + (-i*2*PI/N)*I;
		feh = cexp(feh);
		y[i] = cabs(g[i] + (h[i]*feh))/(N/2);
	}
}
