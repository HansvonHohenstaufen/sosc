/* See LICENSE file for copyright and license details. */

#include <sys/time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <termios.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "serial.h"

/* Variables */
static int serial_fp;

/* Functions imlementations */
void
serial_clean()
{
	tcdrain(serial_fp);
	tcflush(serial_fp,TCIOFLUSH);
}

void
serial_closeport()
{
	close(serial_fp);
}

int
serial_openport(const char *Serial_P, const unsigned int Bauds)
{
	serial_fp = open(Serial_P, O_RDWR|O_NOCTTY);

	if (serial_fp==-1)
		return 1;

	fcntl(serial_fp, F_SETFL,0);

	/* Interface to asynchronous communications */
	struct termios options;
	tcgetattr(serial_fp, &options);

	/* Clear all options */
	memset(&options, 0, sizeof(options));

	speed_t Speed;

	switch (Bauds)
	{
		case    110 :	Speed=B110;    break;
		case    300 :	Speed=B300;    break;
		case    600 :	Speed=B600;    break;
		case   1200 :	Speed=B1200;   break;
		case   2400 :	Speed=B2400;   break;
		case   4800 :	Speed=B4800;   break;
		case   9600 :	Speed=B9600;   break;
		case  19200 :	Speed=B19200;  break;
		case  38400 :	Speed=B38400;  break;
		case  57600 :	Speed=B57600;  break;
		case 115200 :	Speed=B115200; break;
		default     :   return 2;
	}

	cfsetispeed(&options, Speed);
	cfsetospeed(&options, Speed);

	/* Configure serial port: 8bits, no parity, no control */
	options.c_cflag |= (CLOCAL|CREAD|CS8);
	options.c_iflag |= (IGNPAR|IGNBRK );

	/* Timer unsused */
	options.c_cc[VTIME] = 5;

	/* At least on character before satisfy reading */
	options.c_cc[VMIN] = 0;

	tcflush(serial_fp, TCIFLUSH);

	/* Activate the settings */
	tcsetattr(serial_fp, TCSANOW, &options);

	return 0;
}

int
serial_readchar(char *Buffer, const unsigned int NbBytes)
{
	if (!read(serial_fp,Buffer,NbBytes))
		return 1;
	return 0;
}

int
serial_writechar(const char *Buffer, const unsigned int NbBytes)
{
	if (write(serial_fp,Buffer,NbBytes)!=(ssize_t)NbBytes)
		return 1;
	return 0;
}
