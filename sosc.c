/* See LICENSE file for copyright and license details. */

#include <math.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <semaphore.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char*argv0;
#include "serial.h"
#include "drw.h"
#include "fft.h"

#include "arg.h"
#include "util.h"

#include "config.h"

typedef struct Parameters Parameters;
struct Parameters {
	int time;
	float voltage;
	float frequency;
};

typedef struct Measures Measures;
struct Measures {
	float vp;
	float vpp;
	float vm;
	float f;
	char t[9];
	float v1;
	float v2;
	float v3;
	float f2;
	float f3;
};

/* Functions declarations */
static void screen(void);

static void timedec(void);
static void timeinc(void);
static void updateparameters(void);

static void getmeasures(void);

static void *calfft(void *args);
static void *listenport(void *args);
static void *readdata(void *args);

static void configure(void);

static void usage(void);

/* Globals */
static pthread_t threadserial, threaddata, threadfft;
static Parameters parameters;
static Measures measures;

static char *serialdevice = NULL;
static int serialbaud = 0;

static int mode = 0;

static int viewdata[N];
static int viewdatafft[N/2];
static int dataone = 0;
static int dataraw[N];

static float fs = 0.0;		/* Sample frequency */

static sem_t mutexready, mutexdata, mutexmeas, mutexfft;

/* Functions implementations */
void
screen()
{
	char c;
	int exit;
	int dataready;

	exit = 0;
	dataready = 0;

	while (!exit)
	{
		/* Check for command */
		c = getch();

		/* Commands */
		switch (c)
		{
		case 'h':
			if (!mode)
				timedec();
			break;
		case 'l':
			if (!mode)
				timeinc();
			break;
		case 'm':
			mode = !mode;
			break;
		case 'q':
			exit = 1;
		default:
			break;
		}

		sem_getvalue(&mutexdata, &dataready);
		if (dataready>0)
		{
			sem_wait(&mutexdata);
			if (mode)
				drw_data(viewdatafft);
			else
				drw_data(viewdata);
		}

		sem_getvalue(&mutexmeas, &dataready);
		if (dataready>0)
		{
			sem_wait(&mutexmeas);
			drw_measure(	measures.vp,
					measures.vpp,
					measures.vm,
					measures.f,
					measures.t,
					measures.v1,
					measures.v2,
					measures.v3,
					measures.f2,
					measures.f3
					);
			updateparameters();
		}
	}
}

void
changemode()
{
}

void
timedec()
{
	if (parameters.time<=-1)
		return;
	parameters.time--;
}

void
timeinc()
{
	if (1<=parameters.time)
		return;
	parameters.time++;
}

void
updateparameters()
{
	char u;
	char unit[10], volt[10], freq[10];
	int time = 0;

	/* Time units */
	if (0<=parameters.time)
	{
		u = ' ';
		time = pow(10, parameters.time);
	}
	if ((-1<=parameters.time)&&(parameters.time<0))
	{
		u = 'm';
		time = pow(10, 3+parameters.time);
	}

	sprintf(unit, "%3d %cs", time%1000, u);
	unit[6] = '\0';

	/* Voltage units */
	sprintf(volt, "%3.2f V", parameters.voltage);

	/* Frequency */
	sprintf(freq, "%4.1f Hz", parameters.frequency);

	/* Send */
	drw_parameters(unit, volt, freq);
}

void
getmeasures()
{
	int i;
	int ivp, ivpp;

	int ready;

	ivp = 0;
	ivpp = 65535;

	/* FFT */
	sem_getvalue(&mutexfft, &ready);
	if (ready==0)
		sem_post(&mutexfft);

	/* Voltage measures */
	for (i=0; i<N; i++)
	{
		if (ivp<dataraw[i])
			ivp = dataraw[i];
		if (dataraw[i]<ivpp)
			ivpp = dataraw[i];
	}

	ivpp = ivp-ivpp;
	measures.vp  = parameters.voltage*ivp/65535;
	measures.vpp = parameters.voltage*ivpp/65535;

	/* Print screen */
	sem_getvalue(&mutexmeas, &ready);
	if (ready==0)
		sem_post(&mutexmeas);
}

void *
calfft(void *args)
{
	int i;
	int f1;
	int lin, col;
	float datafft[N];
	float v[N/2], v1, t;
	float ym;
	char u;

	/* Init */
	drw_getwindowsize(&col, &lin);
	ym = (lin-1)/parameters.voltage;

	while (1)
	{
		/* Check data ready */
		sem_wait(&mutexfft);

		/* Copy data */
		for (i=0; i<N; i++)
			datafft[i] = dataraw[i]*parameters.voltage/65535;

		/* Get frequency */
		fft_calculate(datafft, v);

		/* Mode */
		if (mode)
			for (i=0; i<N/2; i++)
				viewdatafft[i] = lin - v[i]*ym+1;
		t = fs*col/N;
		if ((t<10.0)||(200.0<t))
			parameters.frequency = 0.0;
		else
			parameters.frequency = t;

		/* DC */
		measures.vm = v[0];

		/* First harmonic */
		v1 = 0.0;
		f1 = 0;
		for (i=1; i<N/2; i++)
			if (v1<v[i])
			{
				f1 = i;
				v1 = v[i];
			}
		measures.v1 = v[f1];
		v[f1] = 0.0;

		measures.f = fs*f1/N;
		t = 1/measures.f;

		/* Time units */
		u = ' ';
		if ((1e-3<t)&&(t<1.0))
		{
			u = 'm';
			t = t*1e3;
		}

		sprintf(measures.t, "%4.1f %cs", t, u);

		/* Second harmonic */
		v1 = 0.0;
		f1 = 0;
		for (i=1; i<N/2; i++)
			if (v1<v[i])
			{
				f1 = i;
				v1 = v[i];
			}
		measures.v2 = v[f1];
		v[f1] = 0.0;

		measures.f2 = fs*f1/N;

		/* Third harmonic */
		v1 = 0.0;
		f1 = 0;
		for (i=1; i<N/2; i++)
			if (v1<v[i])
			{
				f1 = i;
				v1 = v[i];
			}
		measures.v3 = v[f1];

		measures.f3 = fs*f1/N;
	}

	pthread_exit(NULL);
	return NULL;
}

void *
listenport(void *args)
{
	char c;
	char mess[7];

	int i;
	int num, ready;

	while (1)
	{
		/* Clear */
		c = '\0';
		memset(&mess[0], 0, sizeof(mess));

		while (serial_readchar(&c, sizeof(c)));

		if (c!='(')
			continue;

		for (i=0; i<6; i++)
		{
			while (serial_readchar(&c, sizeof(c)));
			mess[i] = c;
		}

		/* Check end data */
		if (c!=')')
			break;
		mess[5] = '\0';

		/* Check value */
		num = atoi(mess);
		if ((num<0)||(65535<num))
			break;

		/* Data ready */
		dataone = num;

		sem_getvalue(&mutexready, &ready);
		if (ready==0)
			sem_post(&mutexready);
	}

	pthread_exit(NULL);
	return NULL;
}

void *
readdata(void *args)
{
	float ym;
	int i, n, nraw;
	int lin, col;
	int dt, sdt, dts;

	int ready;

	struct timeval t0, t1, ts;

	/* Init */
	drw_getwindowsize(&col, &lin);
	ym = (lin-1)/(float)(65536);

	for (i=0; i<N; i++)
	{
		viewdata[i] = lin;
		dataraw[i] = 0;
	}

	for (i=0; i<N/2; i++)
		viewdatafft[i] = lin;

	n = 0;
	nraw = 0;

	gettimeofday(&t0, NULL);

	while (1)
	{
		/* Check data ready */
		sem_wait(&mutexready);

		/* Scale value */
		sdt = 1e6*pow(10, parameters.time)/col;

		/* Time between samples in us */
		gettimeofday(&t1, NULL);
		dt = (t1.tv_sec-t0.tv_sec)*1e6 + (t1.tv_usec-t0.tv_usec);

		/* Add raw data */
		nraw++;
		if (N<nraw)
			nraw = 0;
		dataraw[nraw] = dataone;

		/* Get sample frequency */
		dts = (t1.tv_sec-ts.tv_sec)*1e6 + (t1.tv_usec-ts.tv_usec);
		fs = 1e6/(float)dts;

		gettimeofday(&ts, NULL);

		if (dt<sdt)
			continue;

		/* Add data */
		viewdata[n] = lin-dataraw[nraw]*ym;

		n++;
		if (col<n)
			n = 0;

		/* Print screen */
		sem_getvalue(&mutexdata, &ready);
		if (ready==0)
			sem_post(&mutexdata);

		/* Measures */
		getmeasures();

		gettimeofday(&t0, NULL);
	}

	pthread_exit(NULL);
	return NULL;
}

void
configure()
{
	/* Parameters */
	parameters.time = 0;
	parameters.frequency = 0;

	/* Measures*/
	measures.vp = 0;
	measures.vpp = 0;
	measures.vm = 0;

	measures.v1 = 0;
	measures.v2 = 0;
	measures.v3 = 0;

	measures.f = 0;
	measures.f2 = 0;
	measures.f3 = 0;

	updateparameters();
}

void
usage()
{
	die(	"usage: sosc [OPTION]\n"
		"  -b		Baudrate\n"
		"  -d		Device\n"
		"  -h		Help command\n"
		"  -v		Show version\n"
		"  -x		Max voltage\n"
	);
}

int
main(int argc, char *argv[])
{
	/* Arguments */
	ARGBEGIN {
	case 'b':
		serialbaud = atoi(EARGF(usage()));
		break;
	case 'd':
		serialdevice = EARGF(usage());
		break;
	case 'v':
		die("sosc-"VERSION"\n");
	case 'x':
		parameters.voltage = atof(EARGF(usage()));
		break;
	default:
		usage();
	} ARGEND

	/* Serial port configuration */
	if (!serialbaud)
		serialbaud = 9600;

	if (!serialdevice)
		die("no device\n");

	if ((parameters.voltage<1.0)||(100.0<parameters.voltage))
		parameters.voltage = default_voltage;

	/* Open port */
	if (serial_openport(serialdevice, serialbaud))
		die("%s no connect\n", serialdevice);

	/* Start draw */
	if (drw_start())
	{
		drw_end();
		die("window size is too bit\n");
	}

	/* Start configure */
	configure();

	/* Start semaphores */
	sem_init(&mutexready, 0, 0);
	sem_init(&mutexdata, 0, 0);
	sem_init(&mutexmeas, 0, 0);
	sem_init(&mutexfft, 0, 0);

	/* Thread -> listen to serial port */
	if (pthread_create(&threadserial, NULL, listenport, NULL))
		die("error to create a thread for listen port\n");

	/* Thread -> read data */
	if (pthread_create(&threaddata, NULL, readdata, NULL))
		die("error to create a thread for read data\n");

	/* Thread -> calculte fft */
	if (pthread_create(&threadfft, NULL, calfft, NULL))
		die("error to create a thread for read data\n");

	/* Input command */
	screen();

	/* Kill all threads */
	pthread_cancel(threadserial);
	pthread_cancel(threaddata);

	/* Close */
	serial_closeport();

	/* Close draw*/
	drw_end();

	return 0;
}
