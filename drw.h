/* See LICENSE file for copyright and license details. */

#include <ncurses.h>

/* Variables */
#define	COL	COLS-1
#define	LINE	LINES-1

/* Functions declarations */
void drw_data(int *data);
void drw_window(void);

void drw_getwindowsize(int *col, int *lin);
void drw_measure(float vp, float vpp, float vm,
		float f, char *t,
		float v1, float v2, float v3,
		float f2, float f3);
void drw_parameters(char *time, char *volt, char *f);

void drw_end(void);
int drw_start(void);
