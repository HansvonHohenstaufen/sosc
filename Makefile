# sosc - simple oscilloscope

include config.mk

SRC = sosc.c drw.c fft.c serial.c util.c
OBJ = ${SRC:.c=.o}

all: options sosc

options:
	@echo sosc build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

sosc: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f sosc ${OBJ} sosc-${VERSION}.tar.xz

dist: clean
	mkdir -p sosc-${VERSION}
	cp -R ${SRC} config.h \
		arg.h drw.h fft.h serial.h util.h \
		Makefile config.mk \
		sosc.1 \
		LICENSE README \
		\sosc-${VERSION}
	tar -cf sosc-${VERSION}.tar sosc-${VERSION}
	xz -9 -T0 sosc-${VERSION}.tar
	rm -rf sosc-${VERSION}

install: all
	mkdir -p ${PREFIX}/bin
	cp -f sosc ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/sosc
	mkdir -p ${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < sosc.1 > ${MANPREFIX}/man1/sosc.1

uninstall:
	rm -r ${PREFIX}/bin/sosc \
		${MANPREFIX}/man1/logo.1

.PHONY: all options clean dist install uninstall
